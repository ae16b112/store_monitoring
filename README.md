# store_monitoring

| Service name | store_monitoring |
|------|-----|
| Language/Framework| Python/Django |
| Database | sqlite3 |

## Problem Statement: 
It's important to keep track of the store's online status. All restaurants are expected to have their online presence during their operating hours. However, a store may become inactive for a few hours due to unknown causes. The restaurant owners require a report on how frequently this has occurred in the past. 

## Database Schema:
### The schema includes four models:

* Store: contains information about the store such as id, name, address, timezone, created_at, and updated_at.

* StoreStatus: tracks the status of the store (active/inactive) at a specific timestamp in UTC. It has a foreign key reference to Store model, and its created_at field records when it was added to the database.

* StoreBusinessHours: records the start and end time of the store's business hours for each day of the week. It has a foreign key reference to the Store model and also includes the created_at and updated_at fields.

* Report: used for generating reports on store status. It has an id field, status field to track its current state (running/complete), a JSONField to store report data, and created_at/updated_at fields to record when it was created or updated.

## EndPoints
* `/trigger_report` : endpoint that will trigger report generation from the data provided (stored in DB) 
    - No input
    - Output - report_id (random string) 
    - report_id will be used for polling the status of report completion

* `/get_report` : endpoint that will return the status of the report or the csv
    - Input - report_id
    - Output:
        - if report generation is not complete, return “running” as the output
        - if report generation is complete, return “Complete” along with the CSV file with the schema described above.

## Schema for /get_report api response
```store_id, uptime_last_hour(in percentage), uptime_last_day(in percentage), uptime_last_week(in percentage), downtime_last_hour(in percentage), downtime_last_day(in percentage), downtime_last_week(in percentage)```


## Explanation
`/trigger_report` API calls ```generate_report()``` which is a celery task

1. ```generate_report()``` calls ```generate_and_store_report()```, this method  this method fetched store based on store_id, this method generates a random string report_id, insert a row in report table with values {id, "running"}. It then gets all the stores and generates report one by one for each. Once the report is generated for all the store_ids, it updates the status of report_id in report_status with {report_id, "completed"} and data as json. Data here contains data for each store.

2. ```generate_and_store_report()```: Since we have to generate report for last week for each storeId, we are fetching the business_hours of storeId for each each in last week, if data for any day is missing, enrich the data with startTime 00:00:00 and endTime 23:59:59. Now fetch all the status of this storeId within a week and call ```calculate_weekly_observation_and_generate_report()``` to generate the report for this storeId and insert the record in report table.

3. ```calculate_weekly_observation_and_generate_report``` Firstly, I segmented the business hours into hourly chunks and assigned each chunk with a value of None. Next, I traversed through the list of store_id statuses and checked if the status time falls within the business hours of that day. If it does, I updated the corresponding chunk with either an active or inactive status. After that, I utilized the enrich_status_map_with_nearest_status() method to fill in the remaining chunks with their nearest status. Finally, I used the create_weekly_observation() function to generate the weekly report and returned it.

4. ```enrich_status_map_with_nearest_status()``` We need to update the status of each business hour by replacing any None status with either an active or inactive status. In cases where there are no active or inactive statuses on a given day, I updated each business hour's status by randomly generating a value between 0 and 1, with a status of active assigned if the value is greater than or equal to 0.5, and inactive if the value is less than 0.5.

For days that have some active and inactive statuses, I replaced any None statuses with the nearest active or inactive chunk's status. To determine the starting reference point for this replacement, I also utilized a random value generation approach.

5. ```create_weekly_observation()```, now we have active or inactive status for every chunk of each day, simply used (no. of active chunks / no. of total chunks)*100 to generate uptime percentage of last hour, last day and last week.


`/get_report:report_id` API calls GetCSVData(), it fetches status of report_id from report_status table, if the status is "running", it returns the status and if the status is "completed", fetches report for each store_id corresponding to the report_id from report table and return the report which later get converted to csv file and response is returned.

## Local Setup
1. Clone the repository
   ```git clone https://gitlab.com/ae16b112/store_monitoring.git```
   
2. Create Virtual Environment in the project root
   ```python3 -m venv .venv```
3. Activate the Virtual Environment
   ```source .venv/bin/activate```

4. Install all dependencies
   ```pip3 install -r requirements.txt```

5. Backend <> Database (Migrations)
   `Makemigrations`: *This command prepares a makemigrations file for our new model, or creates a new migrations file for any changes if the models have been modified. This command does not create or affect these changes to the database.*
   
   `Migrate`: *The migrate command runs the instructions defined in the recent migrations file on the database.*

   a. `python3 manage.py makemigrations`
   b. `python3 manage.py migrate`

6. Dump data from csv to corresponding table ```Store```, ```StoreStatus```, ```StoreBusinessHours```

7. Create a super user for yourself locally
   Make sure you remember the password and username!
   `python3 manage.py createsuperuser`

8. Start the server locally
   `python3 manage.py runserver`

9. run celery ```celery -A core worker -l INFO```

10. Visit the admin panel http://127.0.0.1:8000/admin/

