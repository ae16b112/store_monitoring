import uuid
from django.db import models

class Store(models.Model):
    id = models.CharField(max_length=255, unique=True, primary_key=True)
    name = models.CharField(max_length=255)
    address = models.TextField(null=True, blank=True)
    timezone_str = models.CharField(max_length=255, default='America/Chicago')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    
    class Meta:
        verbose_name = "Store"
        verbose_name_plural = "Stores"

    def __str__(self):
        return "{id} - {name} - {timezone_str}".format(id=self.id, name=self.name, timezone_str=self.timezone_str)
    

class StoreStatus(models.Model):
    STATUS = (
        ('active', 'Active'),
        ('inactive', 'Inactive')
    )
    store = models.ForeignKey(Store, on_delete=models.CASCADE)
    timestamp_utc = models.DateTimeField()
    status = models.CharField(max_length=10, choices=STATUS)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = "Store Status"
        verbose_name_plural = "Store Status"

    def __str__(self):
        return "{store} - {status}".format(store=self.store.name, status=self.status)
    

class StoreBusinessHours(models.Model):
    WEEKDAYS = (
        (0, 'Monday'),
        (1, 'Tuesday'),
        (2, 'Wednesday'),
        (3, 'Thursday'),
        (4, 'Friday'),
        (5, 'Saturday'),
        (6, 'Sunday')
    )
    store = models.ForeignKey(Store, on_delete=models.CASCADE)
    day_of_week = models.IntegerField(choices=WEEKDAYS, null=True, blank=True)
    start_time_local = models.TimeField(default='00:00:00')
    end_time_local = models.TimeField(default='23:59:59')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = "Store Business Hours"
        verbose_name_plural = "Store Business Hours"

    def __str__(self):
        return "{store} - {day_of_week}".format(store=self.store.name, day_of_week=self.day_of_week)

class Report(models.Model):
    STATUS = (
        ('running', 'Running'),
        ('complete', 'Complete')
    )
    id = models.CharField(max_length=255, unique=True, primary_key=True)
    status = models.CharField(max_length=10, choices=STATUS)
    data = models.JSONField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = "Report"
        verbose_name_plural = "Reports"

    def __str__(self):
        return "{id} - {status}".format(id=self.id, status=self.status)