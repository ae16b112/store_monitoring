from rest_framework import serializers
from report.models import Store, StoreStatus, StoreBusinessHours,Report

class StoreSerializer(serializers.ModelSerializer):
    class Meta:
        model = Store
        fields = "__all__"
        
class StoreStatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = StoreStatus
        fields = "__all__"
        
        
class StoreBusinessHoursSerializer(serializers.ModelSerializer):
    class Meta:
        model = StoreBusinessHours
        fields = "__all__"
        
        
class ReportSerializer(serializers.ModelSerializer):
    class Meta:
        model = Report
        fields = "__all__"