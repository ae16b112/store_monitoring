from datetime import datetime, timedelta
from report.models import Store, StoreBusinessHours, StoreStatus, Report
import pytz
import random
from report.utils import ConvertUTCToLocal, get_day_mapping, CheckUTCTimeLiesBetweenTwoLocalTime, convert_utc_to_local

def generate_and_store_report(report_id):
    try:
        all_stores=Store.objects.all()
        res={}
        for store in all_stores:
            try: 
                store_id=store.id
                print("processing for store_id: ", store.id)           
                # Get the current datetime in the store's timezone
                # end_time = datetime.now(pytz.timezone(store.timezone_str))
                end_time=datetime(2023, 1, 24, tzinfo=pytz.timezone(store.timezone_str))
                start_time=end_time-timedelta(weeks=1)
                
                # Get the store's business hours
                business_hours = StoreBusinessHours.objects.filter(store=store)
                if business_hours.exists():
                    # Get the store's status observations within the last week, and filter by business hours
                    store_statuses = StoreStatus.objects.filter(
                        store=store,
                        timestamp_utc__range=(start_time, end_time),
                        status='active',
                    ).exclude(
                        timestamp_utc__time__lt=min(bh.start_time_local for bh in business_hours),
                        timestamp_utc__time__gt=max(bh.end_time_local for bh in business_hours),
                    )
                    storeDayTimeMapping, err = fill_business_hours_gaps_and_return_date_time_mapping(store_id, business_hours)
                    data = calculate_weekly_observation_and_generate_report(
                        store.id,
                        store_statuses,
                        business_hours,
                        store.timezone_str,
                        storeDayTimeMapping,
                        end_time
                    )
                    res[store.id]=data
            except Exception as e:
                # print("error: ", e)
                pass
        report=Report.objects.get(id=report_id)
        report.data=res
        report.status='complete'
        report.save()
        return True
    except:
        return False
    
def calculate_weekly_observation_and_generate_report(
    storeId,
    storeStatuses,
    storeBusinessHours,
    timeZone,
    storeDayTimeMapping,
    end_time
):
    statusMap = {}
    for businessHour in storeBusinessHours:
        startTime = businessHour.start_time_local
        endTime = businessHour.end_time_local
        day = businessHour.day_of_week
        totalHourChunks = calculate_total_chunks(startTime.strftime("%H:%M:%S"), endTime.strftime("%H:%M:%S"))
        statusMap[day] = {i: None for i in range(totalHourChunks)}
    
    for storeStatus in storeStatuses:
        localTime, dayStr, err = ConvertUTCToLocal(storeStatus.timestamp_utc, timeZone)
        if err:
            return None, err
        day = get_day_mapping(dayStr)

        liesBetween, err = CheckUTCTimeLiesBetweenTwoLocalTime(storeDayTimeMapping[day].get("start_time_local"), storeDayTimeMapping[day].get("end_time_local"), localTime, timeZone)
        if err:
            return None, err
        if not liesBetween:
            continue

        chunkNumber = get_chunk_number_from_end(storeDayTimeMapping[day].get("end_time_local"), localTime)
        statusMap[day][chunkNumber] = storeStatus.status    
    
    statusMap = enrich_status_map_with_nearest_status(statusMap)

    localTime, dayStr, err = convert_utc_to_local(end_time.astimezone(pytz.utc), timeZone)
    if err:
        return None, err
    day = get_day_mapping(dayStr)

    weeklyObservation = create_weekly_observation(storeId, statusMap, day)
    report = generate_weekly_report(storeId, weeklyObservation, day)
    return report

def generate_weekly_report(store_id, observations, current_day):
    uptime_last_hour, uptime_last_day, uptime_last_week, downtime_last_hour, downtime_last_day, downtime_last_week = 0.0, 0.0, 0.0, 0.0, 0.0, 0.0

    if observations["is_last_hour_active"]:
        uptime_last_hour = 100.0
        downtime_last_hour = 0.0
    else:
        downtime_last_hour = 100.0
        uptime_last_hour = 0.0

    total_weekly_chunks, total_last_day_chunks, total_active_weekly_chunks, total_active_last_day_chunks = 0, 0, 0, 0

    for observation in observations["week_report"]:
        total_weekly_chunks += observation["total_chunks"]
        total_active_weekly_chunks += observation["active_chunks"]
        if current_day == observation["day"]:
            total_last_day_chunks = observation["total_chunks"]
            total_active_last_day_chunks = observation["active_chunks"]

    uptime_last_day = (total_active_last_day_chunks / total_last_day_chunks) * 100 if total_last_day_chunks > 0 else 0.0
    downtime_last_day = ((total_last_day_chunks - total_active_last_day_chunks) / total_last_day_chunks) * 100 if total_last_day_chunks > 0 else 0.0

    uptime_last_week = (total_active_weekly_chunks / total_weekly_chunks) * 100 if total_weekly_chunks > 0 else 0.0
    downtime_last_week = ((total_weekly_chunks - total_active_weekly_chunks) / total_weekly_chunks) * 100 if total_weekly_chunks > 0 else 0.0

    return {
        "store_id":store_id,
        "uptime_last_day":int(uptime_last_day),
        "uptime_last_hour":int(uptime_last_hour),
        "uptime_last_week":int(uptime_last_week),
        "downtime_last_day":int(downtime_last_day),
        "downtime_last_hour":int(downtime_last_hour),
        "downtime_last_week":int(downtime_last_week),
    }

def create_weekly_observation(store_id, status_map, current_day):
    weekly_observation = []

    is_last_hour_active = False

    for key, val in status_map.items():
        total_chunks = len(val)
        active_status = 0

        for chunk, status in val.items():
            if status == "active":
                active_status += 1
                if key == current_day and chunk == 0:
                    is_last_hour_active = True

        day_observation = {
            "day":key,
            "total_chunks":total_chunks,
            "active_chunks":active_status
        }

        weekly_observation.append(day_observation)

    return {
        "store_id":store_id,
        "week_report":weekly_observation,
        "is_last_hour_active":is_last_hour_active  
    }

def enrich_status_map_with_nearest_status(status_map):
    result_map = {}
    for key, val in status_map.items():
        last_status = "inactive"
        r_val = random.uniform(0, 1)
        if r_val >= 0.5:
            last_status = "active"
        last_status_index = 0
        count_active = 0
        count_inactive = 0
        temp_map = {}
        for i in range(len(val)):
            if val.get(i) == "inactive":
                count_inactive += 1
            elif val.get(i) == "active":
                count_active += 1

        if count_active == 0 and count_inactive == 0:
            for i in range(len(val)):
                rand_val = random.uniform(0, 1)
                if rand_val >= 0.5:
                    temp_map[i] = "active"
                else:
                    temp_map[i] = "inactive"

        for i in range(len(val)):
            if val.get(i) != None:
                last_status = val[i]
                last_status_index = i
                temp_map[i] = val[i]
            else:
                for j in range(i + 1, len(val)):
                    if j - i > i - last_status_index:
                        temp_map[i] = last_status
                        break
                    else:
                        if val.get(j) != None:
                            if j - i == i - last_status_index:
                                if count_inactive > count_active:
                                    temp_map[i] = "inactive"
                                else:
                                    temp_map[i] = "active"
                                break
                            temp_map[i] = val[j]
                            break
                else:
                    if count_inactive > count_active:
                        temp_map[i] = "inactive"
                    else:
                        temp_map[i] = "active"
        result_map[key] = temp_map
    return result_map

def fill_business_hours_gaps_and_return_date_time_mapping(storeId, businessHours):
    storeDayTimeMapping = {}

    daysPresent = {}
    for businessHour in businessHours:
        dayOfWeek = businessHour.day_of_week
        daysPresent[dayOfWeek] = True
        storeDayTimeMapping[dayOfWeek] = {
            "start_time_local":(businessHour.start_time_local).strftime("%H:%M:%S"),
            "end_time_local":(businessHour.end_time_local).strftime("%H:%M:%S"),
        }

    for i in range(0, 7):
        present = daysPresent.get(i, False)
        if not present:
            businessHour = StoreBusinessHours.objects.create(
                store=Store.objects.get(id=storeId),
                day_of_week=i,
                start_time_local='00:00:00',
                end_time_local='23:59:59',
            )
            storeDayTimeMapping[i] = {
                "start_time_local":(businessHour.start_time_local).strftime("%H:%M:%S"),
                "end_time_local":(businessHour.end_time_local).strftime("%H:%M:%S"),
            }

    return storeDayTimeMapping, None
    
def calculate_total_chunks(start_time_str, end_time_str):
    try:
        start_time = datetime.strptime(start_time_str, "%H:%M:%S")
    except ValueError:
        return -1
    
    try:
        end_time = datetime.strptime(end_time_str, "%H:%M:%S")
    except ValueError:
        return -1
    
    duration = end_time - start_time
    total_minutes = int(duration.total_seconds() / 60)
    
    if total_minutes % 60 == 0:
        return total_minutes // 60
    
    return total_minutes // 60 + 1

def get_chunk_number_from_end(end_time_str, input_time_str):
    try:
        input_time = datetime.strptime(input_time_str, "%H:%M:%S")
    except ValueError:
        return -1
    
    try:
        end_time = datetime.strptime(end_time_str, "%H:%M:%S")
    except ValueError:
        return -1
    
    minutes = int((end_time - input_time).total_seconds() / 60)
    chunk = minutes // 60
    
    return chunk
