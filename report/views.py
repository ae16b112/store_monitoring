import uuid
from django.http import HttpResponse
from django.core.exceptions import ObjectDoesNotExist
from report.models import Store, StoreStatus, StoreBusinessHours, Report
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from report.tasks import generate_report
import csv

@api_view(['POST'])
def trigger_report(request):
    try:
        # store_id=request.query_params.get("store_id")
        report_id = str(uuid.uuid4())
        report = Report(id=report_id, status='running')
        report.save()
        generate_report.delay(report_id)
        return HttpResponse(report_id)
    except Exception as exception:
        return Response({'error': str(exception)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

@api_view(['GET'])
def get_report(request):
    try:
        report_id = request.query_params.get('report_id')
        try:
            report = Report.objects.get(id=report_id)
        except ObjectDoesNotExist:
            return HttpResponse('Report not found')
        if report.status == 'running':
            return HttpResponse('Running')
        elif report.status == 'complete':
            data = report.data
            if data is None:
                return HttpResponse('No data')
            response = HttpResponse(content_type='text/csv')
            response['Content-Disposition'] = 'attachment; filename="data.csv"'
            writer = csv.writer(response)
            header = ['store_id', 'uptime_last_hour(%)', 'uptime_last_day(%)', 'uptime_last_week(%)', 'downtime_last_hour(%)', 'downtime_last_day(%)', 'downtime_last_week(%)']
            writer.writerow(header)
            for key, val in data.items():
                store_id=val.get("store_id")
                uptime_last_hour=val.get("uptime_last_hour")
                uptime_last_day=val.get("uptime_last_day")
                uptime_last_week=val.get("uptime_last_week")
                downtime_last_hour=val.get("downtime_last_hour")
                downtime_last_day=val.get("downtime_last_day")
                downtime_last_week=val.get("downtime_last_week")
                writer.writerow([store_id, uptime_last_hour, uptime_last_day, uptime_last_week,downtime_last_hour, downtime_last_day,downtime_last_week ])
            return response
    except Exception as exception:
        return Response({'error': str(exception)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
