# tasks.py

from __future__ import absolute_import

from celery import shared_task

from report.helpers import generate_and_store_report

@shared_task
def generate_report(report_id):
    try:
        generate_and_store_report(report_id)
        return True
    except:
        return False
