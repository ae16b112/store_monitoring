from django.contrib import admin
from report.models import Store, StoreStatus, StoreBusinessHours,Report

# Register your models here.

class StoreAdmin(admin.ModelAdmin):
    readonly_fields = ["id"]
    search_fields = ["name"]
    list_display = ['name','timezone_str','created_at']
    
class StoreStatusAdmin(admin.ModelAdmin):
    search_fields = ["store","status"]
    list_display = ["store", "status", "timestamp_utc"]
    
class StoreBusinessHoursAdmin(admin.ModelAdmin):
    search_fields = ["store"]
    list_display = ['store','day_of_week','start_time_local', 'end_time_local']
    
class ReportAdmin(admin.ModelAdmin):
    search_fields = ["id"]
    list_display = ["id", "status"]
    


admin.site.register(Store, StoreAdmin)
admin.site.register(StoreStatus, StoreStatusAdmin)
admin.site.register(StoreBusinessHours, StoreBusinessHoursAdmin)
admin.site.register(Report, ReportAdmin)