from django.urls import path
from rest_framework import routers

from report import views
router = routers.SimpleRouter()

urlpatterns = [
    path('trigger_report', views.trigger_report, name='trigger_report'),
    path('get_report', views.get_report, name='get_report'),
]
urlpatterns += router.urls