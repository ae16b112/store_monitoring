from datetime import datetime, timedelta
from django.db.models import Q
import pytz


def ConvertUTCToLocal(utcTime, timezone):
    # Set the timezone
    tz = pytz.timezone(timezone)

    # Convert the UTC time to the specified timezone
    localTime = utcTime.astimezone(tz)

    # Determine the day of the week
    day = localTime.strftime("%A")

    return localTime.strftime("%H:%M:%S"), day, None

def get_day_mapping(day):
    days_map = {
        "Monday": 0,
        "Tuesday": 1,
        "Wednesday": 2,
        "Thursday": 3,
        "Friday": 4,
        "Saturday": 5,
        "Sunday": 6
    }
    return days_map.get(day, -1)

def CheckUTCTimeLiesBetweenTwoLocalTime(startTimeStr, endTimeStr, localTimeStr, timezone):
    try:
        tz = pytz.timezone(timezone)

        # Parse the start, end, and local time
        startTime = datetime.strptime(startTimeStr, '%H:%M:%S').time()
        endTime = datetime.strptime(endTimeStr, '%H:%M:%S').time()
        localTime = datetime.strptime(localTimeStr, '%H:%M:%S').time()

        # Convert local time to timezone
        localTime = tz.localize(datetime.combine(datetime.today(), localTime)).time()

        if startTime <= localTime <= endTime:
            return True, None
        else:
            return False, None
    except pytz.UnknownTimeZoneError:
        # log error
        return False, pytz.UnknownTimeZoneError
    except ValueError:
        # log error
        return False, ValueError

def convert_utc_to_local(utc_time, timezone):
    # Parse the UTC time
    utc_dt = datetime.strptime(utc_time.strftime("%Y-%m-%d %H:%M:%S.%f"), "%Y-%m-%d %H:%M:%S.%f")

    # Convert the UTC time to the specified timezone
    tz = pytz.timezone(timezone)
    local_dt = utc_dt.replace(tzinfo=pytz.utc).astimezone(tz)

    # Determine the day of the week
    day = local_dt.strftime("%A")

    return local_dt.strftime("%H:%M:%S"), day, None

        
        

